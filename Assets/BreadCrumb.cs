﻿using System;

public class BreadCrumb
{
    public Tuple<int, int> From { get; set; }
    public Tuple<int, int> To { get; set; }
    public Line Line { get; set; }

    public bool IsMatch(Tuple<int, int> from, Tuple<int, int> to)
    {
        return From.Equals(to) && To.Equals(from);
    }
}