﻿
public interface IMaze
{
    bool FoundCheese();

    bool Move(Direction direction);
}