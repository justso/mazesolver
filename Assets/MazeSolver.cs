﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Experimental.UIElements;
using UnityEngine.UI;
using System.Linq;
using Random = UnityEngine.Random;

public class MazeSolver : MonoBehaviour
{
    public Maze Maze;
    bool _working;
    bool _paused;
    bool _doStep;
    bool _dfs = true;
    float _speed = .02f;

    public Text solveText;

    public Direction GetOppositeOf(Direction direction)
    {
        switch (direction)
        {
            case Direction.Up:
                return Direction.Down;
            case Direction.Right:
                return Direction.Left;
            case Direction.Down:
                return Direction.Up;
            case Direction.Left:
                return Direction.Right;
        }
        throw new NotImplementedException("GetOppositeOf(Direction) received a unrecognized Direction.");
    }

    IEnumerator FindCheeseDepthFirst(IMaze maze)
    {
        var theStack = new Stack<Tuple<Tuple<int, int>, List<Direction>, Direction>>();
        var position = new Tuple<int, int>(0, 0);
        var unexploredDirections =
            new List<Direction>() {Direction.Up, Direction.Down, Direction.Left, Direction.Right};
        var visitedSet = new HashSet<Tuple<int, int>> {position};

        while (!maze.FoundCheese())
        {
            if (unexploredDirections.Count > 0)
            {
                var index = Random.Range(0, unexploredDirections.Count);
                var direction = unexploredDirections[index];
                unexploredDirections.RemoveAt(index);
                var x = position.Item1 + (direction == Direction.Left ? -1 : direction == Direction.Right ? 1 : 0);
                var y = position.Item2 + (direction == Direction.Up ? 1 : direction == Direction.Down ? -1 : 0);
                var newPosition = new Tuple<int, int>(x, y);
                if (visitedSet.Contains(newPosition))
                    continue;
                if (maze.Move(direction)) //we were able to move
                {
                    theStack.Push(new Tuple<Tuple<int, int>, List<Direction>, Direction>(position,unexploredDirections, direction));
                    position = newPosition;
                    unexploredDirections = new List<Direction>() { Direction.Up, Direction.Down, Direction.Left, Direction.Right };
                    visitedSet.Add(position);
                }
            }
            else
            {
                var temp = theStack.Pop();
                position = temp.Item1;
                unexploredDirections = temp.Item2;
                maze.Move(GetOppositeOf(temp.Item3));
            }

            _working = true;
            if (_paused) yield return new WaitUntil(Next);
            if (!_paused) yield return new WaitForSeconds(_speed);
        }
        Debug.Log("Success!");

        _working = false;
        yield return null;
    }

    IEnumerator FindCheeseBreadthFirst(IMaze maze)
    {
        var theQueue = new Queue<Tuple<Tuple<int, int>, List<Direction>>>();
        var position = new Tuple<int, int>(0, 0);
        var howToGetHere = new List<Direction>();
        //theQueue.Enqueue(new Tuple<Tuple<int, int>, List<Direction>>(position, howToGetHere));
        var visitedSet = new HashSet<Tuple<int, int>> { position };

        var unexploredDirections =
            new List<Direction>() { Direction.Up, Direction.Down, Direction.Left, Direction.Right };

        while (!maze.FoundCheese())
        {
            if (unexploredDirections.Count > 0)
            {
                var index = Random.Range(0, unexploredDirections.Count);
                var direction = unexploredDirections[index];
                unexploredDirections.RemoveAt(index);
                var x = position.Item1 + (direction == Direction.Left ? -1 : direction == Direction.Right ? 1 : 0);
                var y = position.Item2 + (direction == Direction.Up ? 1 : direction == Direction.Down ? -1 : 0);
                var newPosition = new Tuple<int, int>(x, y);
                if (visitedSet.Contains(newPosition)) continue;
                if (maze.Move(direction))
                {
                    visitedSet.Add(newPosition);
                    var newDirections = new List<Direction>(howToGetHere) {direction};
                    theQueue.Enqueue(new Tuple<Tuple<int, int>, List<Direction>>(newPosition, newDirections));
                    if (!maze.FoundCheese()) maze.Move(GetOppositeOf(direction));
                }
            }
            else
            {
                for (var i = howToGetHere.Count - 1; i >= 0; i--)
                    maze.Move(GetOppositeOf(howToGetHere[i]));

                var temp = theQueue.Dequeue();
                position = temp.Item1;
                howToGetHere = temp.Item2;

                for (var i = 0; i < howToGetHere.Count; i++)
                    maze.Move(howToGetHere[i]);

                unexploredDirections =
                    new List<Direction>() { Direction.Up, Direction.Down, Direction.Left, Direction.Right };
            }

            _working = true;
            if (_paused) yield return new WaitUntil(Next);
            if (!_paused) yield return new WaitForSeconds(_speed);
        }
        Debug.Log("Success!");

        _working = false;
        yield return null;
    }

    bool Next()
    {
        if (!_paused) return true;
        if (!_doStep) return false;
        _doStep = false;
        return true;
    }

    public void Start()
    {
        SetSpeed(.75f);
        Reset();
    }

    public void Reset()
    {
        StopAllCoroutines();
        _working = false;
        Pause();
        Maze.Reset();
    }

    public void Solve()
    {
        if (_paused)
        {
            Unpause();
            if (!_working) StartCoroutine(_dfs ? FindCheeseDepthFirst(Maze) : FindCheeseBreadthFirst(Maze));
        }
        else Pause();
    }

    public void Step()
    {
        Pause();
        if (!_working) StartCoroutine(_dfs ? FindCheeseDepthFirst(Maze) : FindCheeseBreadthFirst(Maze));
        else _doStep = true;
    }

    private void Pause()
    {
        _paused = true;
        solveText.text = "SOLVE";
    }

    private void Unpause()
    {
        _paused = false;
        solveText.text = "PAUSE";
    }

    public void SetSpeed(float value)
    {
        _speed = 1 / (float)Math.Pow(2, 6 * value);
    }

    public void ToggleDFS(int selection)
    {
        _dfs = selection == 0;
        Reset();
    }

}



