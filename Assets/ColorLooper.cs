﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public static class ColorLooper
{
    static float r;
    static float g;
    static float b;
    static int levels = 10;

    static int numberOfColors = levels * 2 * 3;
    static int index = 0;

    public static Color GetNextColor()
    {
        Color color = Colors().ToList()[index];
        index = (index + 1) % numberOfColors;
        return color;
    }

    public static Color GetColor(int i)
    {
        Color color = Colors().ToList()[i % numberOfColors];
        return color;
    }

    static IEnumerable<Color> Colors()
    {
        foreach (var tuple in ColorTween(levels))
            yield return new Color(tuple.Item1, tuple.Item2, 0);
        foreach (var tuple in ColorTween(levels))
            yield return new Color(0, tuple.Item1, tuple.Item2);
        foreach (var tuple in ColorTween(levels))
            yield return new Color(tuple.Item2, 0, tuple.Item1);
    }

    static IEnumerable<Tuple<float, float>> ColorTween(int levelsPerHue)
    {
        float fromHue = 1;
        float toHue = 0;

        for (int i = 0; i < levelsPerHue; i++)
        {
            toHue = (float)i / levelsPerHue;
            yield return new Tuple<float, float>(fromHue, toHue);
        }
        for (int i = levelsPerHue; i > 0; i--)
        {
            fromHue = (float)i / levelsPerHue;
            yield return new Tuple<float, float>(fromHue, toHue);
        }
    }

}
