﻿using System;
using System.Collections.Generic;
using System.Linq;

public class Cell
{
    List<Direction> UnexploredDirections = new List<Direction>();
    public Direction BackTrackDirection { get; set; }
    public Tuple<int, int> RelativePosition { get; set; }

    public bool CanStillExplore { get { return UnexploredDirections.Count > 0; } }

    public Direction GetUnexploredDirection()
    {
        if (UnexploredDirections.Count > 0)
        {
            //Direction direction = UnexploredDirections[0];
            int index = UnityEngine.Random.Range(0, UnexploredDirections.Count);
            Direction direction = UnexploredDirections[index];
            UnexploredDirections.RemoveAt(index);
            return direction;
        }
        else throw new Exception("All directions explored");
    }

    public Cell()
    {
        RelativePosition = new Tuple<int, int>(0, 0);
        foreach (Direction d in Enum.GetValues(typeof(Direction)))
        {
            UnexploredDirections.Add(d);
        }
    }

    public Cell(Direction arrivalDirection, Tuple<int, int> relativePosition)
    {
        BackTrackDirection = arrivalDirection;
        RelativePosition = relativePosition;
        foreach (Direction direction in Enum.GetValues(typeof(Direction)))
        {
            if (direction != arrivalDirection)
                UnexploredDirections.Add(direction);
        }
    }

    internal void MarkDirectionAsExplored(Direction direction)
    {
        if (UnexploredDirections.Contains(direction))
            UnexploredDirections.Remove(direction);
    }
}