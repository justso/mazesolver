﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Line
{

    //NOTE: line length is currently 100 pixels
    protected RectTransform rectTransform;

    public Line(Transform parentCanvasTransform, Vector2 p1, Vector2 p2, Color color, float thickness = 1)
    {
        rectTransform = Object.Instantiate(Resources.Load("Line", typeof(RectTransform)), parentCanvasTransform) as RectTransform;
        ChangeColor(color);
        //HACK HACK HACK TODO: Improve. Does weird things with the circles at the ends of the line.
        rectTransform.localScale = new Vector3(1, thickness, 1);
        rectTransform.GetChild(0).localScale = new Vector3(thickness, 1, thickness);
        rectTransform.GetChild(1).localScale = new Vector3(thickness, 1, thickness);
        MoveTo(p1, p2);
    }

    public void MoveTo(Vector2 p1, Vector2 p2)
    {
        float length = (p2 - p1).magnitude;
        float rotation = Vector2.SignedAngle(Vector2.right, p2 - p1);
        rectTransform.anchoredPosition = p1;
        rectTransform.sizeDelta = new Vector2(length, rectTransform.sizeDelta.y);
        rectTransform.localRotation = Quaternion.Euler(0, 0, rotation);
    }

    public void MoveEndTo(Vector2 p2)
    {
        Vector2 p1 = rectTransform.localPosition;
        MoveTo(p1, p2);
    }

    public void ChangeColor(Color color)
    {
        foreach (Image image in rectTransform.GetComponentsInChildren<Image>())
            image.color = color;
    }

    public void MoveToBack()
    {
        rectTransform.SetAsFirstSibling();
    }

    public void Delete(float timer = 0)
    {
        Object.Destroy(rectTransform.gameObject, timer);
    }

    public void SetInvisibility(bool invisible)
    {
        rectTransform.gameObject.SetActive(!invisible);
    }
}
