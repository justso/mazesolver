﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Maze : MonoBehaviour, IMaze
{
    int _startPosition;
    int _currentPosition;
    Tuple<int, int> CurrentPositionVector => GetPositionVector(_currentPosition);
    int _end;
    List<Edge> _perfectMazeEdges;
    List<Tuple<Edge,Line>> _extraEdges;
    float _wallDensity = .5f;
    int NumberOfExtraEdges => (int) ((N - 1) * (N - 1) * (1-_wallDensity));

    float _borderWidth; //border width
    float _cellWidth; //width of each cell
    public int N = 10; //size of maze
    public InputField SizeInputField;

    Stack<BreadCrumb> _breadCrumbs;
    Color BreadCrumbColor => ColorLooper.GetColor(_breadCrumbs.Count);
    Color BacktrackColor => new Color(.9f, .9f, .9f);
    private float LineWeight => 5f / N;

    public GameObject Lines;
    public GameObject BreadCrumbLines;

    public void ToggleEdges(float wallDensity)
    {
        if (wallDensity > _wallDensity)
        {
            Reset();
        }
        _wallDensity = wallDensity;
        for (var i = 0; i < _extraEdges.Count; i++)
        {
            _extraEdges[i].Item2.SetInvisibility(i < NumberOfExtraEdges);
        }
    }

    public void Start()
    {
        New();
    }

    public void UpdateSize(string value)
    {
        int.TryParse(value, out N);
        if (N < 2)
        {
            N = 2;
            SizeInputField.text = "2";
        }
        if (N > 20)
        {
            N = 20;
            SizeInputField.text = "20";
        }
        New();
    }

    public void New()
    {
        Destroy(Lines);
        Lines = new GameObject("Lines");
        Lines.transform.SetParent(transform, worldPositionStays: false);

        _breadCrumbs = new Stack<BreadCrumb>();

        _borderWidth = 400 / 8; //VALUES???
        _cellWidth = (400 - 2 * _borderWidth) / N; //VALUES???

        _perfectMazeEdges = MazeHelper.GenerateLongPathMaze(N, out _startPosition, out _end);
        _extraEdges = new List<Tuple<Edge, Line>>();
        CreateAndDrawFullMaze();

        DrawStartAndFinish();
        DrawBorder();

        Reset();
    }

    public void Reset()
    {
        _currentPosition = _startPosition;

        Destroy(BreadCrumbLines);
        BreadCrumbLines = new GameObject("BreadCrumbLines");
        BreadCrumbLines.transform.SetParent(transform, worldPositionStays: false);

        _breadCrumbs = new Stack<BreadCrumb>();

    }

    public bool FoundCheese()
    {
        return _currentPosition == _end;
    }

    public bool Move(Direction direction)
    {
        var nextPositionVector = new Tuple<int, int>(
            CurrentPositionVector.Item1 + (direction == Direction.Left ? -1 : direction == Direction.Right ? 1 : 0),
            CurrentPositionVector.Item2 + (direction == Direction.Down ? -1 : direction == Direction.Up ? 1 : 0));
        if (EdgeExists(CurrentPositionVector, nextPositionVector))
        {
            if (_breadCrumbs.Count > 0 && _breadCrumbs.Peek().IsMatch(CurrentPositionVector, nextPositionVector))
            {
                var line = _breadCrumbs.Pop().Line;
                line.ChangeColor(BacktrackColor);
                line.MoveToBack();
            }
            else
            {
                _breadCrumbs.Push(new BreadCrumb() {
                    From = CurrentPositionVector,
                    To = nextPositionVector,
                    Line = DrawBreadCrumb(CurrentPositionVector, nextPositionVector) });
            }
            _currentPosition = PositionToSingleInt(nextPositionVector);
            return true;
        }
        else
        {
            DrawBreadCrumb(CurrentPositionVector, nextPositionVector, blocked: true);
            return false;
        }
    }

    void DrawStartAndFinish()
    {
        var x = (GetColumn(_startPosition) + .5f - N / 2f) * _cellWidth;
        var y = (GetRow(_startPosition) + .5f - N / 2f) * _cellWidth;
        var pos = new Vector2(x, y);
        new Line(Lines.transform, pos, pos, Color.red, LineWeight);
        x = (GetColumn(_end) + .5f - N / 2f) * _cellWidth;
        y = (GetRow(_end) + .5f - N / 2f) * _cellWidth;
        pos = new Vector2(x, y);
        new Line(Lines.transform, pos, pos, Color.black, LineWeight);
    }

    void DrawBorder()
    {
        float p = _cellWidth * N / 2; //for readability of next four lines
        new Line(Lines.transform, new Vector2(-p, -p), new Vector2(-p, p), Color.black, LineWeight);
        new Line(Lines.transform, new Vector2(-p, -p), new Vector2(p, -p), Color.black, LineWeight);
        new Line(Lines.transform, new Vector2(p, p), new Vector2(-p, p), Color.black, LineWeight);
        new Line(Lines.transform, new Vector2(p, p), new Vector2(p, -p), Color.black, LineWeight);
    }

    void CreateAndDrawFullMaze()
    {
        foreach (var edge in MazeHelper.GetAllPossibleEdges(N).Except(_perfectMazeEdges))
        {
            var i = edge.Vertices.Item1;
            var j = edge.Vertices.Item2;
            float x = (GetColumn(i) + .5f - N / 2f) * _cellWidth;
            float y = (GetRow(i) + .5f - N / 2f) * _cellWidth;
            Line line = null;
            if (j - i == 1)
                line = new Line(Lines.transform, new Vector2(x + _cellWidth / 2, y - _cellWidth / 2), new Vector2(x + _cellWidth / 2, y + _cellWidth / 2), Color.black, LineWeight); //vertical line to the right
            else if (j - i == N)
                line = new Line(Lines.transform, new Vector2(x - _cellWidth / 2, y + _cellWidth / 2), new Vector2(x + _cellWidth / 2, y + _cellWidth / 2), Color.black, LineWeight); //horizontal line above
            _extraEdges.Add(new Tuple<Edge, Line>(edge, line));
        }

        ToggleEdges(_wallDensity);
    }

    Line DrawBreadCrumb(Tuple<int,int> start, Tuple<int, int> end, bool blocked = false)
    {
        float x1 = (start.Item1 + .5f - N / 2f) * _cellWidth;
        float y1 = (start.Item2 + .5f - N / 2f) * _cellWidth;
        Vector2 startPoint = new Vector2(x1, y1);
        float x2 = (end.Item1 + .5f - N / 2f) * _cellWidth;
        float y2 = (end.Item2 + .5f - N / 2f) * _cellWidth;
        Vector2 endPoint = new Vector2(x2, y2);

        if (blocked)
        {
            startPoint = startPoint + (endPoint - startPoint) * .25f; //just a piece of it, in the direction of the attempted move
            endPoint = startPoint + (endPoint - startPoint) * 0f; //just a piece of it, in the direction of the attempted move
        }

        return new Line(BreadCrumbLines.transform, startPoint, endPoint, blocked ? BacktrackColor : BreadCrumbColor, LineWeight);
    }


    int PositionToSingleInt(Tuple<int, int> a) => a.Item2 * N + a.Item1;

    bool EdgeExists(Tuple<int, int> a, Tuple<int, int> b) => EdgeExists(PositionToSingleInt(a), PositionToSingleInt(b));

    bool EdgeExists(int a, int b)
    {
        var e = new Edge(a, b);
        return _perfectMazeEdges.Contains(e) || _extraEdges.Take(NumberOfExtraEdges).Select(o=>o.Item1).Contains(e);
    }

    Tuple<int, int> GetPositionVector(int positionAsSingleInt) => new Tuple<int, int>(GetColumn(positionAsSingleInt), GetRow(positionAsSingleInt));

    int GetColumn(int positionAsSingleIn) => positionAsSingleIn % N;

    int GetRow(int positionAsSingleIn) => positionAsSingleIn / N;
}