﻿using UnityEngine;
using UnityEngine.EventSystems;

public class BasicInteractiveElementScript : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler, IBeginDragHandler, IDragHandler, IEndDragHandler, IDropHandler
{
    GameObject me;

    void Awake()
    {
        me = gameObject;
    }

	public delegate void PointerEventHandler(GameObject o, PointerEventData eventData);

    public event PointerEventHandler PointerDown;
    public event PointerEventHandler PointerUp;
    public event PointerEventHandler PointerEnter;
	public event PointerEventHandler PointerExit;
	public event PointerEventHandler BeginDrag;
	public event PointerEventHandler Drag;
	public event PointerEventHandler EndDrag;
	public event PointerEventHandler Drop;

	public void OnPointerDown(PointerEventData eventData)
    {
        if (PointerDown != null) PointerDown(me, eventData);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (PointerUp != null) PointerUp(me, eventData);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (PointerEnter != null) PointerEnter(me, eventData);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (PointerExit != null) PointerExit(me, eventData);
    }

	public void OnBeginDrag(PointerEventData eventData)
	{
		if (BeginDrag != null) BeginDrag(me, eventData);
	}

	public void OnDrag(PointerEventData eventData)
	{
		if (Drag != null) Drag(me, eventData);
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		if (EndDrag != null) EndDrag(me, eventData);
	}

    public void OnDrop(PointerEventData eventData)
    {
        if (Drop != null) Drop(me, eventData);
    }


}
