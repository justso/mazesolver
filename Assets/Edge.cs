﻿using System;

public struct Edge : IEquatable<Edge>
{
    public Tuple<int, int> Vertices { get; }

    public Edge(int x, int y)
    {
        Vertices = new Tuple<int, int>(x, y);
    }

    public override bool Equals(object obj) => obj is Edge && Equals((Edge) obj);

    public bool Equals(Edge e)
    {
        return
            (Vertices.Item1 == e.Vertices.Item1 && Vertices.Item2 == e.Vertices.Item2) ||
            (Vertices.Item1 == e.Vertices.Item2 && Vertices.Item2 == e.Vertices.Item1);
    }

    public override int GetHashCode() => Vertices.Item1 ^ Vertices.Item2;

    public static bool operator ==(Edge lhs, Edge rhs) => lhs.Equals(rhs);

    public static bool operator !=(Edge lhs, Edge rhs) => !(lhs.Equals(rhs));
}