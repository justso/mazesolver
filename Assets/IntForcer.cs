﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class IntForcer : MonoBehaviour
{

    public InputField Field;

    public void Validate(string value)
    {
        if (value.Contains("-"))
            Field.text = value.Replace("-", "");

        else
        {
            int size;
            int.TryParse(value, out size);
            //if (size < 1)
            //    Field.text = "1";
            if (size > 20)
                Field.text = "20";
        }
    }

    public void ValidateSize(string value)
    {
    }

}
