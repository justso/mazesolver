﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;
using Random = UnityEngine.Random;

public class MazeHelper
{
    public static List<Edge> GenerateLongPathMaze(int n, out int start, out int end)
    {
        start = Random.Range(0, n * n);
        var currentPoint = start;
        end = start;
        var distanceToEnd = 0;
        var distanceTracker = 0;
        var algorithmStack = new List<int>();
        var alreadyInMaze = new List<int>() {start};
        var mazeEdges = new List<Edge>(); //edge in the graph theoretic sense, i.e. represents an available path
        while (alreadyInMaze.Count < n * n)
        {
            var availableNeighbours = GetAvailableNeighbors(currentPoint, n, alreadyInMaze).ToList();
            if (availableNeighbours.Count > 0)
            {
                var newPoint = availableNeighbours[Random.Range(0, availableNeighbours.Count)];
                mazeEdges.Add(new Edge(currentPoint, newPoint));
                algorithmStack.Add(currentPoint);
                currentPoint = newPoint;
                alreadyInMaze.Add(newPoint);
                distanceTracker++;
                if (distanceTracker <= distanceToEnd) continue;
                end = newPoint;
                distanceToEnd = distanceTracker;
            }
            else
            {
                currentPoint = algorithmStack.Last();
                algorithmStack.Remove(algorithmStack.Last());
                distanceTracker--;
            }
        }

        return mazeEdges;
    }

    public static List<int> GetAvailableNeighbors(int v, int n, List<int> alreadyInMaze)
    {
        var neighbors = new List<int>();
        var i = v % n;
        var j = v / n;
        if (i > 0 && !alreadyInMaze.Contains(v - 1)) neighbors.Add(v - 1);
        if (i < n - 1 && !alreadyInMaze.Contains(v + 1)) neighbors.Add(v + 1);
        if (j > 0 && !alreadyInMaze.Contains(v - n)) neighbors.Add(v - n);
        if (j < n - 1 && !alreadyInMaze.Contains(v + n)) neighbors.Add(v + n);
        return neighbors;
    }

    public static List<Edge> GetAllPossibleEdges(int n)
    {
        var edges = new List<Edge>();
        for (var i = 0; i < n; i++)
        {
            for (var j = 0; j < n; j++)
            {
                if (i < n - 1) edges.Add(new Edge(i + j * n, i + j * n + 1));
                if (j < n - 1) edges.Add(new Edge(i + j * n, i + j * n + n));
            }
        }
        Shuffle(edges);
        return edges;
    }

    static void Shuffle<T>([NotNull] List<T> listToShuffle)
    {
        if (listToShuffle == null) throw new ArgumentNullException(nameof(listToShuffle));
        var n = listToShuffle.Count;
        for (var i = 0; i < n; i++)
        {
            var j = Random.Range(0, n);
            var temp = listToShuffle[i];
            listToShuffle[i] = listToShuffle[j];
            listToShuffle[j] = temp;
        }
    }
}
